# Tarte au citron Media Remote Video
Submodule of [Tarte au citron](https://www.drupal.org/project/tarte_au_citron) module.

Comply to the European cookie law using [tarteaucitron.js](https://github.com/AmauriC/tarteaucitron.js) with Remote videos provider from media core module.

## Installation

1. Install Tarte au citron and Media modules
2. Add Media type "Remote videos" using remote video provider
2. Download the module and enable it
4. Configure at Administer > Configuration >
Tarte au citron > Settings for Tarte au citron module.


### Composer installation
1. Add the module
```bash
composer require drupal/tarte_au_citron_media_remote_video
